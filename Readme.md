# ghello-go

GO言語で作った Google App Engine アプリケーションです。

----

  1. このソースのインストール

	    $ git clone https://extra-wasbai-jp@bitbucket.org/extra-wasabi-jp/ghello-go.git

  
  
  1. Goライブラリ(Webフレームワーク)のインストール

    	$ go get github.com/zenazn/goji
  
  
  1. 起動

    	$ cd apps
    	$ golang serve --host=0.0.0.0

    ※ http://localhost:8080/ で表示
    

  1. デプロイ

    	$ gcloud app deploy

    ※ Linux に gcloud app platform がインストールされていることを前提にしています。

    ちなみにこんな環境

	    [sakizawa@webdev2 ghello-go]$ gcloud --version
    	Google Cloud SDK 145.0.0
    	app-engine-go
    	app-engine-go-linux-x86_64 1.9.50
    	app-engine-php " "
    	app-engine-python 1.9.50
    	beta 2016.01.12
    	bq 2.0.24
    	bq-nix 2.0.24
    	core 2017.02.21
    	core-nix 2016.11.07
    	docker-credential-gcr
    	docker-credential-gcr-linux-x86_64 1.3.2
    	gcloud
    	gcloud-deps 2017.02.21
    	gcloud-deps-linux-x86_64 2017.02.21
    	gsutil 4.22
    	gsutil-nix 4.20
    	kubectl
    	kubectl-linux-x86_64 1.5.2      
        
    	[sakizawa@webdev2 ghello-go]$ gcloud components list
        
    	Your current Cloud SDK version is: 145.0.0
    	The latest available version is: 149.0.0
        
    	lqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqk
    	x                                                   Components                                                   x
    	tqqqqqqqqqqqqqqqqqqwqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqwqqqqqqqqqqqqqqqqqqqqqqqqqqwqqqqqqqqqqqu
    	x      Status      x                         Name                         x            ID            x    Size   x
    	tqqqqqqqqqqqqqqqqqqnqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqnqqqqqqqqqqqqqqqqqqqqqqqqqqnqqqqqqqqqqqu
    	x Update Available x BigQuery Command Line Tool                           x bq                       x   < 1 MiB x
    	x Update Available x Cloud SDK Core Libraries                             x core                     x   5.8 MiB x
    	x Update Available x Cloud Storage Command Line Tool                      x gsutil                   x   2.8 MiB x
    	x Update Available x gcloud Beta Commands                                 x beta                     x   < 1 MiB x
    	x Update Available x gcloud app Python Extensions                         x app-engine-python        x   6.1 MiB x
    	x Not Installed    x Bigtable Command Line Tool                           x cbt                      x   3.9 MiB x
    	x Not Installed    x Cloud Datalab Command Line Tool                      x datalab                  x   < 1 MiB x
    	x Not Installed    x Cloud Datastore Emulator                             x cloud-datastore-emulator x  15.4 MiB x
    	x Not Installed    x Cloud Datastore Emulator (Legacy)                    x gcd-emulator             x  38.1 MiB x
    	x Not Installed    x Cloud Pub/Sub Emulator                               x pubsub-emulator          x  21.0 MiB x
    	x Not Installed    x Emulator Reverse Proxy                               x emulator-reverse-proxy   x  56.8 MiB x
    	x Not Installed    x gcloud Alpha Commands                                x alpha                    x   < 1 MiB x
    	x Not Installed    x gcloud app Java Extensions                           x app-engine-java          x 128.6 MiB x
    	x Installed        x App Engine Go Extensions                             x app-engine-go            x  47.9 MiB x
    	x Installed        x Default set of gcloud commands                       x gcloud                   x           x
    	x Installed        x Google Container Registry's Docker credential helper x docker-credential-gcr    x   3.4 MiB x
    	x Installed        x kubectl                                              x kubectl                  x  11.5 MiB x
    	mqqqqqqqqqqqqqqqqqqvqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqvqqqqqqqqqqqqqqqqqqqqqqqqqqvqqqqqqqqqqqj
    	To install or remove components at your current SDK version [145.0.0], run:
    	  $ gcloud components install COMPONENT_ID
    	  $ gcloud components remove COMPONENT_ID

    	To update your SDK installation to the latest version [149.0.0], run:
    	  $ gcloud components update

  1. GAE へのデプロイ準備（要確認）
    go ライブラリを ghello-go 直下にコピーする。
    	$ cp $GOPATH/pkg .
    	$ cp $GOPATH/src .

  1. GAE へのデプロイ
    	gcloud app deploy



以上
