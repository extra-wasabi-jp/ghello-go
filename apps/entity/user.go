package entity

import (
    "time"
    "database/sql"
)

type User struct {
    Cust_cd string
    User_cd string
    Eff_beg_dt string
    Eff_end_dt string
    Pwd string
    Pwd_lst_chg_dt string
    Emp_no string
    User_ja_nm string
    User_kn_nm string
    Gender_cd string
    Cont_tel_1 sql.NullString
    Cont_tel_2 sql.NullString
    Cont_email_1 sql.NullString
    Cont_email_2 sql.NullString
    Join_dt string
    Quit_dt sql.NullString
    Reg_user_nm string
    Reg_tmst time.Time
    Mod_user_nm sql.NullString
    Mod_tmst time.Time
}
