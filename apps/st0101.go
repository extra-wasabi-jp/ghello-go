package main

import (
    "fmt"
    "time"
    "net/http"
    "html/template"
    "github.com/zenazn/goji"
    "github.com/zenazn/goji/web"
//    "github.com/coopernurse/gorp"
//    "entity"
    _ "database/sql"
)

func init() {
    goji.Get("/st0101/start", startAction)
    goji.Get("/st0101/regist", registAction)
}

func startAction(c web.C, w http.ResponseWriter, r *http.Request) {

    // UTC -> JST
    jst := time.Now().In(time.FixedZone("Asia/Tokyo", 9*60*60))
    tpl, err1 := template.ParseFiles("view/st0101.html")

    if err1 != nil {
        fmt.Fprintf(w, "%#v", err1)
        panic(err1.Error)
    }

    err2 := tpl.Execute(w, struct {
            Action string
            Title string
            Message string
            Now string
        }{
            Action: "start",
            Title: "GAE テンプレート＋Gojiテスト (st0101/init)",
            Message: "こんにちは from GAE",
            Now: jst.Format("2006/01/02 15:04:05"),
        })
    if err2 != nil {
        fmt.Fprintf(w, "%#v", err2)
        panic(err2.Error)
    }

}

func registAction(c web.C, w http.ResponseWriter, r *http.Request) {

    // UTC -> JST
    jst := time.Now().In(time.FixedZone("Asia/Tokyo", 9*60*60))
    tpl, err1 := template.ParseFiles("view/st0101.html")

    if err1 != nil {
        fmt.Fprintf(w, "%#v", err1)
        panic(err1.Error)
    }

    err2 := tpl.Execute(w, struct {
            Action string
            Title string
            Message string
            Now string
        }{
            Action: "regist",
            Title: "GAE テンプレート＋Gojiテスト (st0101/regist)",
            Message: "こんにちは from GAE",
            Now: jst.Format("2006/01/02 15:04:05"),
        })
    if err2 != nil {
        fmt.Fprintf(w, "%#v", err2)
        panic(err2.Error)
    }

}
