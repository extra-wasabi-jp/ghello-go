package main

import (
    "fmt"
    "time"
    "net/http"
    "html/template"
    "github.com/zenazn/goji"
    "github.com/zenazn/goji/web"
//    "github.com/coopernurse/gorp"
//    "entity"
    _ "database/sql"
)

func init() {
    http.Handle("/", goji.DefaultMux)
    goji.Get("/", mainAction)
}

func mainAction(c web.C, w http.ResponseWriter, r *http.Request) {

    // UTC -> JST
    jst := time.Now().In(time.FixedZone("Asia/Tokyo", 9*60*60))
    tpl, err1 := template.ParseFiles("view/start.html")

    if err1 != nil {
        fmt.Fprintf(w, "%#v", err1)
        panic(err1.Error)
    }

    err2 := tpl.Execute(w, struct {
            Title string
            Message string
            Now string
        }{
            Title: "GAE テンプレート＋Gojiテスト (main)",
            Message: "こんにちは from GAE",
            Now: jst.Format("2006/01/02 15:04:05"),
        })
    if err2 != nil {
        fmt.Fprintf(w, "%#v", err2)
        panic(err2.Error)
    }

}
